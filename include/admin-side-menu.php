<?php
/**
 * Created by PhpStorm.
 * User: technicus
 * Date: 28/2/16
 * Time: 1:51 PM
 */
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=$_SESSION['user_name']?></p>
                <a href="../logout.php"> Logout</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="">
                    <i class="fa fa-th"></i> <span>DashBoard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Manage Team</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="add_doctor.php"><i class="fa fa-circle-o"></i> Add Doctor</a></li>
                    <li><a href="manage_doctor.php"><i class="fa fa-circle-o"></i> Manage Doctor</a></li>
                    <li><a href="add_staff.php"><i class="fa fa-circle-o"></i> Add Staff</a></li>
                    <li><a href="manage_staff.php"><i class="fa fa-circle-o"></i> Manage Staff</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Manage Paient</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o"></i> View Patient</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
