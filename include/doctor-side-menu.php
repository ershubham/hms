<?php
/**
 * Created by PhpStorm.
 * User: technicus
 * Date: 28/2/16
 * Time: 1:51 PM
 */
?>
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation">
        <li class="user-details cyan darken-2">
            <div class="row">
                <div class="col col s8 m8 l8">
                    <ul id="profile-dropdown" class="dropdown-content">
                        <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../logout.php"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                        </li>
                    </ul>
                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="dashboard.php#" data-activates="profile-dropdown"><?=$_SESSION['user_name']?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                    <p class="user-roal"><?=$_SESSION['user_type']?></p>
                </div>
            </div>
        </li>
        <li class="bold active"><a href="dashboard.php" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Appointment</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="">View Appointment</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Patient</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="add_doctor.php">Add Doctor</a></li>
                            <li><a href="add_staff">Add Staff </a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
    <a href="dashboard.php#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
</aside>