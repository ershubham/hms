<?php
/**
 * Created by PhpStorm.
 * User: technicus
 * Date: 29/2/16
 * Time: 2:09 AM
 */
include('../include/config.php');
include('../include/header.php');
include('../include/admin-side-menu.php');
?>
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
               <div class="col-lg-10 col-lg-offset-1">
               <h4>Doctor List</h4>
               <hr>
                   <div class="panel panel-default">
                       <div class="panel-body">
                           <table class="table">
                               <tr>
                                   <th colspan="2">Dr. Sammy</th>
                               </tr>
                               <tr>
                                   <th>User ID</th><th>20145</th>
                               </tr>
                               <tr>
                                   <th>Email</th><th>ceo@technicus.in</th>
                               </tr>
                               <tr>
                                   <th>Gender</th><th>Male</th>
                               </tr>
                               <tr>
                                   <th>Contact</th><th>+91-8556093704</th>
                               </tr>
                               <tr>
                                   <th>Speciality</th><th>Child Specialist</th>
                               </tr>
                               <tr>
                                   <th>Data of Joining</th><th>23/03/2014</th>
                               </tr>
                           </table>
                           <table class="table">
                            <tr>
                                   <th> <a href="" class="btn btn-primary">Edit</a> </th>
                                   <th> <a href="" class="btn btn-primary">Delete</a> </th>
                               </tr>
                           </table>
                       </div>
                       </div>
                   </div>
               </div>
            </div>
        </section>
    </div>
<?php
include('../include/footer.php');