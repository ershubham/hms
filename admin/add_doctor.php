<?php
/**
 * Created by PhpStorm.
 * User: technicus
 * Date: 29/2/16
 * Time: 1:31 AM
 */
include('../include/config.php');
include('../include/header.php');
include('../include/admin-side-menu.php');
?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <section class="content">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <?php
                    if (isset($_POST['addDoc'])) {
                        $name = $_POST['name'];
                        $email = $_POST['email'];
                        $pass = md5($_POST['password']);
                        $cont = $_POST['contact'];
                        $spec = $_POST['spec'];
                        $gender = $_POST['gender'];

                        //insert into user
                        $sql="INSERT INTO `user_table`(`user_name`, `user_email`, `user_password`, `user_contact`, `user_type`, `user_reg_date`) VALUES (
  '$name','$email','$pass','$cont','doctor',NOW())";
                        $xc=mysql_query($sql) or die(mysql_error());
                        $doc_id=mysql_insert_id();
                        if($doc_id!="")
                        {
                            //insert into doc
                            $sql1="INSERT INTO `doc_details`(`user_id`, `gender`, `doc_speci`, `doc_doj`) VALUES (
                '$doc_id','$gender','$spec',NOW())";
                            $xcx=mysql_query($sql1) or die(mysql_error());
                            if(mysql_insert_id()!="")
                            {
                                echo '<div class="alet alert-success">Added SuccessFully</div>';
                            }
                        }else
                        {
                            echo '<div class="alet alert-danger">Failed</div>';
                        }
                    }
                    ?>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Doctor</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" required="true"
                                           placeholder="Enter Name">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" required="true"
                                           placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" required="true"
                                           placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label>Contact</label>
                                    <input type="number" class="form-control" name="contact" required="true"
                                           placeholder="Enter number">
                                </div>
                                <div class="form-group">
                                    <label>Specialization</label>
                                    <select name="spec" class="form-control" required="">
                                        <option value="">Select</option>
                                        <option value="child">Child</option>
                                        <option value="Dental">Dental</option>
                                        <option value="Surgury">Surgury</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <input type="radio" name="gender" value="male">Male
                                    <input type="radio" name="gender" value="female">FeMale
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" name="addDoc" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->


<?php
include('../include/footer.php');